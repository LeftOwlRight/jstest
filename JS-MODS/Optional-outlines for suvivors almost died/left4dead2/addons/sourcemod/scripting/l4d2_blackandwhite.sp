//Based off retsam code but i have done a complete rewrite with new ffunctions  and more features
#pragma semicolon 1
#pragma newdecls required
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
//#include <L4D2ModelChanger>

native int LMC_GetClientOverlayModel(int client);// remove this and enable the include to compile with the include this is just here for AM compiler

#define PLUGIN_VERSION "2.0.1"

ConVar hCvar_Enabled;
ConVar hCvar_GlowEnabled;
ConVar hCvar_GlowColour;
ConVar hCvar_GlowRange;
ConVar hCvar_GlowFlash;
ConVar hCvar_NoticeType;
ConVar hCvar_TeamNoticeType;
ConVar hCvar_HintRange;
ConVar hCvar_HintTime;
ConVar hCvar_HintColour;


bool bEnabled = false;
bool bGlowEnabled = false;
int iGlowColour;
int iGlowRange = 1800;
int iGlowFlash = 30;
int iNoticeType = 2;
int iTeamNoticeType = 2;
int iHintRange = 600;
float fHintTime = 5.0;
char sHintColour[17];

char sCharName[17];
char clientName[100];

bool bGlow[MAXPLAYERS+1] = {false, ...};

bool bLMC_Available = false;

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	MarkNativeAsOptional("LMC_GetClientOverlayModel");
	MarkNativeAsOptional("LMC_SetClientOverlayModel");
	MarkNativeAsOptional("LMC_SetEntityOverlayModel");
	MarkNativeAsOptional("LMC_GetEntityOverlayModel");
	MarkNativeAsOptional("LMC_HideClientOverlayModel");
	return APLRes_Success;
}

public void OnAllPluginsLoaded()
{
	bLMC_Available = LibraryExists("L4D2ModelChanger");
}

public void OnLibraryAdded(const char[] sName)
{
	if(StrEqual(sName, "L4D2ModelChanger"))
	bLMC_Available = true;
}

public void OnLibraryRemoved(const char[] sName)
{
	if(StrEqual(sName, "L4D2ModelChanger"))
	bLMC_Available = false;
}

public Plugin myinfo =
{
	name = "LMC_Black_and_White_Notifier",
	author = "Lux",
	description = "Notify people when player is black and white Using LMC model if any",
	version = PLUGIN_VERSION,
	url = "https://forums.alliedmods.net/showthread.php?p=2449184#post2449184"
}

public void OnPluginStart()
{
	CreateConVar("lmc_bwnotice_version", PLUGIN_VERSION, "黑白发光插件版本.", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY|FCVAR_DONTRECORD);
	hCvar_Enabled = CreateConVar("lmc_blackandwhite", "1", "启用幸存者黑白发光插件? 0=禁用,1=启用.", FCVAR_NOTIFY);
	hCvar_GlowEnabled = CreateConVar("lmc_glow", "1", "启用幸存者黑白发光. 0=禁用,1=启用.", FCVAR_NOTIFY);
	hCvar_GlowColour = CreateConVar("lmc_glowcolour", "0 0 255", "设置幸存者黑白后的发光颜色.(0 0 255=蓝色).", FCVAR_NOTIFY);
	hCvar_GlowFlash = CreateConVar("lmc_glowflash", "25", "黑白状态下的幸存者血量低于多少时黑白光环开始闪烁. (0=禁用)", FCVAR_NOTIFY);
	hCvar_GlowRange = CreateConVar("lmc_glowrange", "1600.0", "黑白发光的幸存者最大的可视距离.", FCVAR_NOTIFY);
	hCvar_HintColour = CreateConVar("lmc_hintcolour", "0 0 255", "屏幕中间类或暗示类黑白提示的字体颜色.(0 0 255=蓝色)", FCVAR_NOTIFY);
	hCvar_HintRange = CreateConVar("lmc_hintrange", "1200", "使用暗示类黑白提示时幸存者队友能看见提示消息的距离. 最小值1, 最大值9999.", FCVAR_NOTIFY);
	hCvar_HintTime = CreateConVar("lmc_hinttime", "10.0", "黑白消息的提示持续时间.(单位:秒). 最小值1, 最大值20.", FCVAR_NOTIFY);
	hCvar_NoticeType = CreateConVar("lmc_noticetype", "2", "幸存者黑白后的通知类型. 0= 关闭, 1=聊天窗, 2=屏幕中下+聊天窗, 3=屏幕中下, 4=暗示类提示(如果没有启用这个则看不见提示: 按键盘ESC,选项,游戏提示改成已启用).", FCVAR_NOTIFY);
	hCvar_TeamNoticeType = CreateConVar("lmc_teamnoticetype", "0", "幸存者黑白后通知给谁. (0=幸存者, 1=感染者, 2=幸存者和感染者)", FCVAR_NOTIFY);
	
	HookEvent("revive_success", eReviveSuccess);
	HookEvent("heal_success", eHealSuccess);
	HookEvent("player_death", ePlayerDeath);
	HookEvent("player_spawn", ePlayerSpawn);
	HookEvent("player_team", eTeamChange);
	HookEvent("pills_used", eItemUsedPill);
	HookEvent("adrenaline_used", eItemUsed);
	
	HookConVarChange(hCvar_Enabled, l4d2_eConvarChanged);
	HookConVarChange(hCvar_GlowEnabled, l4d2_eConvarChanged);
	HookConVarChange(hCvar_GlowColour, l4d2_eConvarChanged);
	HookConVarChange(hCvar_GlowRange, l4d2_eConvarChanged);
	HookConVarChange(hCvar_GlowFlash, l4d2_eConvarChanged);
	HookConVarChange(hCvar_NoticeType, l4d2_eConvarChanged);
	HookConVarChange(hCvar_TeamNoticeType, l4d2_eConvarChanged);
	HookConVarChange(hCvar_HintRange, l4d2_eConvarChanged);
	HookConVarChange(hCvar_HintTime, l4d2_eConvarChanged);
	HookConVarChange(hCvar_HintColour, l4d2_eConvarChanged);
	
	//监听give命令.
	AddCommandListener(CommandListener, "give");
	
	AutoExecConfig(true, "l4d2_blackandwhite");
	
	l4d2_CvarsChanged();
	
}

public Action CommandListener(int client, const char[] command, int args)
{
	if( args > 0 )
	{
		char buffer[8];
		GetCmdArg(1, buffer, sizeof(buffer));

		if( strcmp(buffer, "health") == 0 )
		{
			RequestFrame(ResetCount, GetClientUserId(client));
		}
	}
}

void ResetCount(any client)
{
	if((client = GetClientOfUserId(client)) && GetEntProp(client, Prop_Send, "m_currentReviveCount") < GetMaxReviveCount())
	{
		static int iEntity;
		iEntity = -1;
		if(bGlowEnabled)
		{
			bGlow[client] = false;
			if(bLMC_Available)
			{
				iEntity = LMC_GetClientOverlayModel(client);
				if(iEntity > MaxClients)
				{
					ResetGlows(iEntity);
				}
				else
				{
					ResetGlows(client);
				}
			}
			else
			{
				ResetGlows(client);
			}
		}
	}
}

public void OnMapStart()
{
	l4d2_CvarsChanged();
}

public void l4d2_eConvarChanged(ConVar convar, const char[] oldValue, const char[] newValue)
{
	l4d2_CvarsChanged();
}

void l4d2_CvarsChanged()
{
	bEnabled = GetConVarInt(hCvar_Enabled) > 0;
	bGlowEnabled = GetConVarInt(hCvar_GlowEnabled) > 0;
	char sGlowColour[13];
	GetConVarString(hCvar_GlowColour, sGlowColour, sizeof(sGlowColour));
	iGlowColour = GetColor(sGlowColour);
	iGlowRange = GetConVarInt(hCvar_GlowRange);
	iGlowFlash = GetConVarInt(hCvar_GlowFlash);
	iNoticeType = GetConVarInt(hCvar_NoticeType);
	iTeamNoticeType = GetConVarInt(hCvar_TeamNoticeType);
	iHintRange = GetConVarInt(hCvar_HintRange);
	fHintTime = GetConVarFloat(hCvar_HintTime);
	GetConVarString(hCvar_HintColour, sHintColour, sizeof(sHintColour));
}

public void eReviveSuccess(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	if(!GetEventBool(event, "lastlife"))
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "subject"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || !IsPlayerAlive(client))
		return;
	
	static int iEntity;
	iEntity = -1;
	
	if(bGlowEnabled)
	{
		bGlow[client] = true;
		if(bLMC_Available)
		{
			iEntity = LMC_GetClientOverlayModel(client);
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_iGlowType", 3);
				SetEntProp(iEntity, Prop_Send, "m_glowColorOverride", iGlowColour);
				SetEntProp(iEntity, Prop_Send, "m_nGlowRange", iGlowRange);
				
			}
			else
			{
				SetEntProp(client, Prop_Send, "m_iGlowType", 3);
				SetEntProp(client, Prop_Send, "m_glowColorOverride", iGlowColour);
				SetEntProp(client, Prop_Send, "m_nGlowRange", iGlowRange);
			}
		}
		else
		{
			SetEntProp(client, Prop_Send, "m_iGlowType", 3);
			SetEntProp(client, Prop_Send, "m_glowColorOverride", iGlowColour);
			SetEntProp(client, Prop_Send, "m_nGlowRange", iGlowRange);
		}
	}
	
	GetModelName(client, iEntity);
	RequestFrame(iTeamNotice, GetClientUserId(client));
}

void iTeamNotice(any client)
{
	if((client = GetClientOfUserId(client)) && GetEntProp(client, Prop_Send, "m_currentReviveCount") == GetMaxReviveCount())
	{
		GetTrueName(client, clientName);
		
		switch(iTeamNoticeType)
		{
			case 0:
			{
				for(int i = 1; i <= MaxClients;i++)
				{
					if(!IsClientInGame(i) || GetClientTeam(client) != 2 || IsFakeClient(i) || i == client)
					continue;
					
					if(iNoticeType == 1 || iNoticeType == 2)
					PrintToChat(i, "\x04[提示]\x03%s\x05已经黑白了,需要治疗.", clientName);
					if(iNoticeType == 2 || iNoticeType == 3)
					PrintHintText(i, "[提示] %s 已经黑白,需要治疗.", clientName);
					if(iNoticeType == 4)
					DirectorHint(client, i);
				}
				
			}
			case 1:
			{
				for(int i = 1; i <= MaxClients;i++)
				{
					if(!IsClientInGame(i) || GetClientTeam(client) != 3 || IsFakeClient(i))
					continue;
					
					if(iNoticeType == 1 || iNoticeType == 2)
					PrintToChat(i, "\x04[提示]\x03%s\x05已经黑白了,需要治疗.", clientName);
					if(iNoticeType == 2 || iNoticeType == 3)
					PrintHintText(i, "[提示] %s 已经黑白,需要治疗.", clientName);
					if(iNoticeType == 4)
					PrintHintText(i, "[提示] %s 已经黑白,需要治疗", clientName);
				}
			}
			case 2:
			{
				for(int i = 1; i <= MaxClients;i++)
				{
					if(!IsClientInGame(i) || IsFakeClient(i) || i == client)
					continue;
					
					if(iNoticeType == 1 || iNoticeType == 2)
					PrintToChat(i, "\x04[提示]\x03%s\x05已经黑白了,需要治疗.", clientName);
					if(iNoticeType == 2 || iNoticeType == 3)
					PrintHintText(i, "[提示] %s 已经黑白,需要治疗.", clientName);
					if(GetClientTeam(i) !=2)
					{
						PrintHintText(i, "[提示] %s 已经黑白,需要治疗.", clientName);
						continue;
					}
					if(iNoticeType == 4)
					DirectorHint(client, i);
				}
			}
		}
	}
}

public void eHealSuccess(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "subject"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || !IsPlayerAlive(client))
		return;
	
	if(!bGlow[client])
		return;
	
	static int iEntity;
	iEntity = -1;
	if(bGlowEnabled)
	{
		bGlow[client] = false;
		if(bLMC_Available)
		{
			iEntity = LMC_GetClientOverlayModel(client);
			if(iEntity > MaxClients)
			{
				ResetGlows(iEntity);
			}
			else
			{
				ResetGlows(client);
			}
		}
		else
		{
			ResetGlows(client);
		}
	}
	
	GetModelName(client, iEntity);
	static int iHealer;
	iHealer = GetClientOfUserId(GetEventInt(event, "userid"));
	GetTrueName(client, clientName);
	switch(iTeamNoticeType)
	{
		case 0:
		{
			for(int i = 1; i <= MaxClients;i++)
			{
				if(!IsClientInGame(i) || GetClientTeam(client) != 2 || IsFakeClient(i) || i == client || i == iHealer)
				continue;
				
				if(iNoticeType == 1 || iNoticeType == 2)
				if(client != iHealer)
				PrintToChat(i, "\x04[提示]\x03%s\x05已接受治疗,不再黑白.", clientName);
				else
				PrintToChat(i, "\x04[提示]\x03%s\x05治疗了自己,不再黑白.", clientName);
				
				if(iNoticeType == 2 || iNoticeType == 3)
				if(client != iHealer)
				PrintHintText(i, "[提示] %s 已接受治疗,不再黑白.", clientName);
				else
				PrintHintText(i, "[提示] %s 治疗了自己,不再黑白.", clientName);
				
				if(iNoticeType == 4)
				DirectorHintAll(client, iHealer, i);
			}
		}
		case 1:
		{
			for(int i = 1; i <= MaxClients;i++)
			{
				if(!IsClientInGame(i) || GetClientTeam(client) != 3 || IsFakeClient(i) || i == client || i == iHealer)
				continue;
				
				if(iNoticeType == 1 || iNoticeType == 2)
				if(client != iHealer)
				PrintToChat(i, "\x04[提示]\x03%s\x05已接受治疗,不再黑白.", clientName);
				else
				PrintToChat(i, "\x04[提示]\x03%s\x05治疗了自己,不再黑白.", clientName);
				
				if(iNoticeType == 2 || iNoticeType == 3)
				if(client != iHealer)
				PrintHintText(i, "[提示] %s 已接受治疗,不再黑白.", clientName);
				else
				PrintHintText(i, "[提示] %s 治疗了自己,不再黑白.", clientName);
				
				if(iNoticeType == 4)
				if(client != iHealer)
				PrintHintText(i, "[提示] %s 已接受治疗,不再黑白.", clientName);
				else
				PrintHintText(i, "[提示] %s 治疗了自己,不再黑白.", clientName);
			}
		}
		case 2:
		{
			for(int i = 1; i <= MaxClients;i++)
			{
				if(!IsClientInGame(i) || IsFakeClient(i) || i == client || i == iHealer)
					continue;
				
				if(iNoticeType == 1 || iNoticeType == 2)
				if(client != iHealer)
					PrintToChat(i, "\x04[提示]\x03%s\x05已接受治疗,不再黑白.", clientName);
				else
					PrintToChat(i, "\x04[提示]\x03%s\x05治疗了自己,不再黑白.", clientName);
				
				if(iNoticeType == 2 || iNoticeType == 3)
				if(client != iHealer)
					PrintHintText(i, "[提示] %s 已接受治疗,不再黑白.", clientName);
				else
					PrintHintText(i, "[提示] %s 治疗了自己,不再黑白.", clientName);
				
				if(GetClientTeam(i) !=2)
				if(client != iHealer)
				{
					PrintHintText(i, "[提示] %s 已接受治疗,不再黑白.", clientName);
					continue;
				}
				else
				{
					PrintHintText(i, "[提示] %s 治疗了自己,不再黑白.", clientName);
					continue;
				}
				if(iNoticeType == 4)
					DirectorHintAll(client, iHealer, i);
			}
		}
	}
}

public void ePlayerDeath(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || GetClientTeam(client) != 2)
		return;
	
	if(!bGlow[client])
		return;
	
	bGlow[client] = false;
	
	if(bLMC_Available)
	{
		static int iEntity;
		iEntity = LMC_GetClientOverlayModel(client);
		if(iEntity > MaxClients)
		{
			ResetGlows(iEntity);
		}
		else
		{
			ResetGlows(client);
		}
	}
	else
	{
		ResetGlows(client);
	}
}

public void ePlayerSpawn(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || GetClientTeam(client) != 2)
		return;
		
	if(GetEntProp(client, Prop_Send, "m_currentReviveCount") < GetMaxReviveCount())
	{
		if(bLMC_Available)
		{
			static int iEntity;
			iEntity = LMC_GetClientOverlayModel(client);
			if(iEntity > MaxClients)
			{
				ResetGlows(iEntity);
			}
			else
			{
				ResetGlows(client);
			}
		}
		else
		{
			ResetGlows(client);
		}
		bGlow[client] = false;
		return;
	}
	
	bGlow[client] = true;
	if(bLMC_Available)
	{
		static int iEntity;
		iEntity = LMC_GetClientOverlayModel(client);
		if(iEntity > MaxClients)
		{
			SetEntProp(iEntity, Prop_Send, "m_iGlowType", 3);
			SetEntProp(iEntity, Prop_Send, "m_glowColorOverride", iGlowColour);
			SetEntProp(iEntity, Prop_Send, "m_nGlowRange", iGlowRange);
			
		}
		else
		{
			SetEntProp(client, Prop_Send, "m_iGlowType", 3);
			SetEntProp(client, Prop_Send, "m_glowColorOverride", iGlowColour);
			SetEntProp(client, Prop_Send, "m_nGlowRange", iGlowRange);
		}
	}
	else
	{
		SetEntProp(client, Prop_Send, "m_iGlowType", 3);
		SetEntProp(client, Prop_Send, "m_glowColorOverride", iGlowColour);
		SetEntProp(client, Prop_Send, "m_nGlowRange", iGlowRange);
	}
}

public void eTeamChange(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || GetClientTeam(client) != 2 || !IsPlayerAlive(client))
		return;
	
	if(bLMC_Available)
	{
		static int iEntity;
		iEntity = LMC_GetClientOverlayModel(client);
		if(iEntity > MaxClients)
		{
			SetEntProp(iEntity, Prop_Send, "m_iGlowType", 0);
			SetEntProp(iEntity, Prop_Send, "m_glowColorOverride", 0);
			SetEntProp(iEntity, Prop_Send, "m_nGlowRange", 0);
			SetEntProp(iEntity, Prop_Send, "m_bFlashing", 0, 1);
		}
		else
		{
			SetEntProp(client, Prop_Send, "m_iGlowType", 0);
			SetEntProp(client, Prop_Send, "m_glowColorOverride", 0);
			SetEntProp(client, Prop_Send, "m_nGlowRange", 0);
			SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
		}
	}
	else
	{
		SetEntProp(client, Prop_Send, "m_iGlowType", 0);
		SetEntProp(client, Prop_Send, "m_glowColorOverride", 0);
		SetEntProp(client, Prop_Send, "m_nGlowRange", 0);
		SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
	}
	
}

public void LMC_OnClientModelApplied(int client, int iEntity, const char sModel[PLATFORM_MAX_PATH], int bBaseReattach)
{
	if(!IsClientInGame(client) || GetClientTeam(client) != 2)
		return;
	
	if(!bGlow[client])
		return;
	
	SetEntProp(iEntity, Prop_Send, "m_iGlowType", GetEntProp(client, Prop_Send, "m_iGlowType"));
	SetEntProp(iEntity, Prop_Send, "m_glowColorOverride", GetEntProp(client, Prop_Send, "m_glowColorOverride"));
	SetEntProp(iEntity, Prop_Send, "m_nGlowRange", GetEntProp(client, Prop_Send, "m_glowColorOverride"));
	SetEntProp(iEntity, Prop_Send, "m_bFlashing", GetEntProp(client, Prop_Send, "m_bFlashing", 1), 1);
	
	SetEntProp(client, Prop_Send, "m_iGlowType", 0);
	SetEntProp(client, Prop_Send, "m_glowColorOverride", 0);
	SetEntProp(client, Prop_Send, "m_nGlowRange", 0);
	SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
}

public void LMC_OnClientModelDestroyed(int client, int iEntity)
{
	if(!IsClientInGame(client) || !IsPlayerAlive(client) || GetClientTeam(client) != 2)
		return;
	
	if(!IsValidEntity(iEntity))
		return;
	
	if(!bGlow[client])
		return;
	
	SetEntProp(client, Prop_Send, "m_iGlowType", GetEntProp(iEntity, Prop_Send, "m_iGlowType"));
	SetEntProp(client, Prop_Send, "m_glowColorOverride", GetEntProp(iEntity, Prop_Send, "m_glowColorOverride"));
	SetEntProp(client, Prop_Send, "m_nGlowRange", GetEntProp(iEntity, Prop_Send, "m_glowColorOverride"));
	SetEntProp(client, Prop_Send, "m_bFlashing", GetEntProp(iEntity, Prop_Send, "m_bFlashing", 1), 1);
}

static void GetModelName(int client, int iEntity)
{
	static char sModel[64];
	if(!IsValidEntity(iEntity))
	{
		GetEntPropString(client, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
		
		if(StrContains(sModel, "teenangst", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Zoey");
		else if(StrContains(sModel, "biker", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Francis");
		else if(StrContains(sModel, "manager", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Louis");
		else if(StrContains(sModel, "namvet", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Bill");
		else if(StrContains(sModel, "producer", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Rochelle");
		else if(StrContains(sModel, "mechanic", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Ellis");
		else if(StrContains(sModel, "coach", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Coach");
		else if(StrContains(sModel, "gambler", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Nick");
		else if(StrContains(sModel, "adawong", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "AdaWong");
		else
			strcopy(sCharName, sizeof(sCharName), "Unknown");
	}
	else if(IsValidEntity(iEntity))
	{
		GetEntPropString(iEntity, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
		
		if(StrContains(sModel, "Bride", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Witch Bride");
		else if(StrContains(sModel, "Witch", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Witch");
		else if(StrContains(sModel, "hulk", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Tank");
		else if(StrContains(sModel, "boomer", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Boomer");
		else if(StrContains(sModel, "boomette", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Female Boomer");
		else if(StrContains(sModel, "hunter", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Hunter");
		else if(StrContains(sModel, "smoker", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Smoker");
		else if(StrContains(sModel, "teenangst", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Zoey");
		else if(StrContains(sModel, "biker", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Francis");
		else if(StrContains(sModel, "manager", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Louis");
		else if(StrContains(sModel, "namvet", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Bill");
		else if(StrContains(sModel, "producer", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Rochelle");
		else if(StrContains(sModel, "mechanic", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Ellis");
		else if(StrContains(sModel, "coach", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Coach");
		else if(StrContains(sModel, "gambler", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Nick");
		else if(StrContains(sModel, "adawong", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "AdaWong");
		else if(StrContains(sModel, "rescue", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Chopper Pilot");
		else if(StrContains(sModel, "common", false) > 0)
			strcopy(sCharName, sizeof(sCharName), "Infected");
		else
			strcopy(sCharName, sizeof(sCharName), "Unknown");
	}
}

static void DirectorHint(int client, int i)
{
	static int iEntity;
	iEntity = CreateEntityByName("env_instructor_hint");
	if(iEntity == -1)
		return;
	GetTrueName(client, clientName);
	static char sValues[51];
	FormatEx(sValues, sizeof(sValues), "hint%d", client);
	DispatchKeyValue(client, "targetname", sValues);
	DispatchKeyValue(iEntity, "hint_target", sValues);
	
	FormatEx(sValues, sizeof(sValues), "%i", iHintRange);
	DispatchKeyValue(iEntity, "hint_range", sValues);
	DispatchKeyValue(iEntity, "hint_icon_onscreen", "icon_alert");
	
	FormatEx(sValues, sizeof(sValues), "%f", fHintTime);
	DispatchKeyValue(iEntity, "hint_timeout", sValues);
	
	FormatEx(sValues, sizeof(sValues), "[提示] %s 已黑白,需要治疗.", clientName);
	DispatchKeyValue(iEntity, "hint_caption", sValues);
	DispatchKeyValue(iEntity, "hint_color", sHintColour);
	DispatchSpawn(iEntity);
	AcceptEntityInput(iEntity, "ShowHint", i);
	
	FormatEx(sValues, sizeof(sValues), "OnUser1 !self:Kill::%f:1", fHintTime);
	SetVariantString(sValues);
	AcceptEntityInput(iEntity, "AddOutput");
	AcceptEntityInput(iEntity, "FireUser1");
}

static void DirectorHintAll(int client, int iHealer, int i)
{
	static int iEntity;
	iEntity = CreateEntityByName("env_instructor_hint");
	if(iEntity == -1)
		return;
	GetTrueName(client, clientName);
	static char sValues[62];
	FormatEx(sValues, sizeof(sValues), "hint%d", i);
	DispatchKeyValue(i, "targetname", sValues);
	DispatchKeyValue(iEntity, "hint_target", sValues);
	
	DispatchKeyValue(iEntity, "hint_range", "0.1");
	DispatchKeyValue(iEntity, "hint_icon_onscreen", "icon_info");
	
	FormatEx(sValues, sizeof(sValues), "%f", fHintTime);
	DispatchKeyValue(iEntity, "hint_timeout", sValues);
	
	if(client == iHealer)
		FormatEx(sValues, sizeof(sValues), "[提示] %s 治疗了自己,不再黑白.", clientName);
	else
		FormatEx(sValues, sizeof(sValues), "[提示] %s 已接受治疗,不再黑白.", clientName);
	
	DispatchKeyValue(iEntity, "hint_caption", sValues);
	DispatchKeyValue(iEntity, "hint_color", sHintColour);
	DispatchSpawn(iEntity);
	AcceptEntityInput(iEntity, "ShowHint", i);
	
	FormatEx(sValues, sizeof(sValues), "OnUser1 !self:Kill::%f:1", fHintTime);
	SetVariantString(sValues);
	AcceptEntityInput(iEntity, "AddOutput");
	AcceptEntityInput(iEntity, "FireUser1");
}

//silvers colour converter
int GetColor(char sTemp[13])
{
	char sColors[3][4];
	ExplodeString(sTemp, " ", sColors, 3, 4);
	
	int color;
	color = StringToInt(sColors[0]);
	color += 256 * StringToInt(sColors[1]);
	color += 65536 * StringToInt(sColors[2]);
	return color;
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public Action OnTakeDamage(int iVictim, int &iAttacker, int &iInflictor, float &fDamage, int &iDamagetype)
{
	if(!bEnabled)
		return;
	
	if(iVictim < 1 || iVictim > MaxClients)
		return;
	
	if(!IsClientInGame(iVictim) || GetClientTeam(iVictim) != 2)
		return;
	
	if(!bGlow[iVictim])
		return;
	
	static int iEntity;
	iEntity = -1;
	if(bLMC_Available)
	iEntity = LMC_GetClientOverlayModel(iVictim);
	
	if(L4D_GetPlayerTempHealth(iVictim) + GetEntProp(iVictim, Prop_Send, "m_iHealth") <= iGlowFlash)
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
			else
			{
				SetEntProp(iVictim, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
		}
		SetEntProp(iVictim, Prop_Send, "m_bFlashing", 1, 1);
		
	}
	else
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
			else
			{
				SetEntProp(iVictim, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
		}
		SetEntProp(iVictim, Prop_Send, "m_bFlashing", 0, 1);
	}
}

static int L4D_GetPlayerTempHealth(int client)
{
	static Handle painPillsDecayCvar = null;
	if (painPillsDecayCvar == null)
	{
		painPillsDecayCvar = FindConVar("pain_pills_decay_rate");
		if (painPillsDecayCvar == null)
		{
			return -1;
		}
	}
	
	int tempHealth = RoundToCeil(GetEntPropFloat(client, Prop_Send, "m_healthBuffer") - ((GetGameTime() - GetEntPropFloat(client, Prop_Send, "m_healthBufferTime")) * GetConVarFloat(painPillsDecayCvar))) - 1;
	return tempHealth < 0 ? 0 : tempHealth;
}
static int GetMaxReviveCount()
{
	static Handle hMaxReviveCount = null;
	if (hMaxReviveCount == null)
	{
		hMaxReviveCount = FindConVar("survivor_max_incapacitated_count");
		if (hMaxReviveCount == null)
		{
			return -1;
		}
	}
	
	return GetConVarInt(hMaxReviveCount);
}

public void eItemUsedPill(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "subject"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || GetClientTeam(client) != 2 || !IsPlayerAlive(client))
		return;
	
	if(!bGlow[client])
		return;
	
	static int iEntity;
	iEntity = -1;
	if(bLMC_Available)
	iEntity = LMC_GetClientOverlayModel(client);
	
	if(L4D_GetPlayerTempHealth(client) + GetEntProp(client, Prop_Send, "m_iHealth") <= iGlowFlash)
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
			else
			{
				SetEntProp(client, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
		}
		SetEntProp(client, Prop_Send, "m_bFlashing", 1, 1);
		
	}
	else
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
			else
			{
				SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
		}
		SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
	}
}

public void eItemUsed(Event event, const char[] name, bool dontBroadcast)
{
	if(!bEnabled)
		return;
	
	static int client;
	client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(client < 1 || client > MaxClients)
		return;
	
	if(!IsClientInGame(client) || GetClientTeam(client) != 2 || !IsPlayerAlive(client))
		return;
	
	if(!bGlow[client])
		return;
	
	static int iEntity;
	iEntity = -1;
	if(bLMC_Available)
	iEntity = LMC_GetClientOverlayModel(client);
	
	if(L4D_GetPlayerTempHealth(client) + GetEntProp(client, Prop_Send, "m_iHealth") <= iGlowFlash)
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
			else
			{
				SetEntProp(client, Prop_Send, "m_bFlashing", 1, 1);
				return;
			}
		}
		SetEntProp(client, Prop_Send, "m_bFlashing", 1, 1);
		
	}
	else
	{
		if(bLMC_Available)
		{
			if(iEntity > MaxClients)
			{
				SetEntProp(iEntity, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
			else
			{
				SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
				return;
			}
		}
		SetEntProp(client, Prop_Send, "m_bFlashing", 0, 1);
	}
}

static void ResetGlows(int iEntity)
{
	SetEntProp(iEntity, Prop_Send, "m_iGlowType", 0);
	SetEntProp(iEntity, Prop_Send, "m_glowColorOverride", 0);
	SetEntProp(iEntity, Prop_Send, "m_nGlowRange", 0);
	SetEntProp(iEntity, Prop_Send, "m_bFlashing", 0, 1);
}

void GetTrueName(int bot, char[] savename)
{
	int tbot = IsClientIdle(bot);
	if(tbot != 0)
	{
		Format(savename, 100, "★闲置:%N★", tbot);
	}
	else
	{
		GetClientName(bot, savename, 100);
	}
}

int IsClientIdle(int bot)
{
	if(IsClientInGame(bot) && GetClientTeam(bot) == 2 && IsFakeClient(bot))
	{
		char sNetClass[12];
		GetEntityNetClass(bot, sNetClass, sizeof(sNetClass));

		if(strcmp(sNetClass, "SurvivorBot") == 0)
		{
			int client = GetClientOfUserId(GetEntProp(bot, Prop_Send, "m_humanSpectatorUserID"));			
			if(client > 0 && IsClientInGame(client))
			{
				return client;
			}
		}
	}
	return 0;
}