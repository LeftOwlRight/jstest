#pragma semicolon 1
//強制1.7以後的新語法
#pragma newdecls required
#include <sourcemod>

public void OnPluginStart()
{
	HookEvent("weapon_reload", Event_ReloadStart);
}

//修复SG552重载弹时的BUG.
public void Event_ReloadStart(Event event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(event.GetInt("userid"));
	int weapon = GetEntPropEnt(client, Prop_Send, "m_hActiveWeapon");
	char classname[128];
	GetEntityClassname(weapon, classname, sizeof(classname));
	if(StrContains(classname, "sg552", false) > -1)
	{
		float nextattack;
		nextattack = GetEntPropFloat(client, Prop_Send, "m_flNextAttack");
		nextattack -= 0.6;
		SetEntPropFloat(client, Prop_Send, "m_flNextAttack", nextattack);
		SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", nextattack);
	}
}