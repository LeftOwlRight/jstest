/*注:.下面有一些本人注释的改法,不会语言的人也可以修改你想要的一些功能,改完之后请重新编译插件!
**有能力的玩家可以用给定的API实现你自己想要的功能,在<binhooks>的inc文件中
**/

/* 本多特只支持110平台以上，相比早期版本，此多特无论在位置优化上，还是卡特上，都有了明显的优化
* 甚至还解决了早期版本跑图无特的问题
* 本扩展多特会针对每一个玩家产生最优位置特感，跑图的，落后的均无法幸免，此扩展刷特很猛，建议时间调长一些
* 避免被虐的体无完肤...
*/

#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <binhooks>
#define PLUGIN_VERSION "8.0.0.0' Mr Cheng"
bool g_TankEIST;
public Plugin myinfo =
{
	name = "多特",
	description = "战役中多特感,解决卡特,ems HUD显示!",
	author = "Mr Cheng",
	version = PLUGIN_VERSION,
	url = "qq群64854516"
};
/*后续还会继续更新如特感智商等等以提高难度，敬请期待....*/

public void OnPluginStart()
{
	CreateConVar("l4d2_SpecialsVersion", PLUGIN_VERSION, "One Specials Plguin.", FCVAR_NOTIFY|FCVAR_DONTRECORD|FCVAR_REPLICATED);
	
	
	SetIsLoadSpecial(true);//false:不加载,true:加载     是否加载多特功能.
	
	/*0:导演引擎模式产生特感,但时间上不准确,这是引擎本身的行为导致的，它就像一个智能AI,
	 *我们无法准确确定它的时间，难度较小,和脚本引擎的产生特感模式几乎一致.
	 *1:为正常化模式产生特感,前几个扩展版本均是这个模式,产生位置经过了深度算法分析和数据结构优化过的,性能上比市面上的
	 *刷特更节约性能,产生的位置将各加合理,难度适中,但比引擎刷特难度更加大.
	 *2:为噩梦模式产生特感,为正常模式和地狱模式的中间位难度,它的刷特地点不同于正常模式和地狱模式,但本质还是经过算法深度分析的,
	 *它将会产生在生还者的周围任何不在幸存者视线内有利于袭击生还者的角落它不再仅仅是产生在障碍物的后面,难度比较大,望谨慎选择!
	 *3:为地狱模式,此模式下特感产生的位置将是无序但有规则的,但本质还是经过算法深度分析的,它将会产生在生还者的周围
	 *任何有利于袭击生还者的角落,它不再仅仅是产生在障碍物的后面,所以难度空前加大,请慎用此模式,非抖M请远离...
	 */
	//  难度:  地狱>噩梦>正常>导演引擎
	/*刷特特点:
	*地狱模式(3):无序刷特,任何幸存者周围的位置包括无视障碍物以及可在幸存者视线范围内...(几乎不卡特感)
	*噩梦模式(2):无序刷特,可在幸存者周围的位置包括无视障碍物但不会在幸存者视线范围内...(几乎不卡特感)
	*正常模式(1):有序刷特,可在幸存者周围的位置且只在幸存者看不见的障碍物后面刷特...(目前只可能在开局有玩家卡加载情况下卡特,详细请看SetIsOnInferno后面注释)
	*导演引擎模(0):有序刷特,可在幸存者周围的位置且只在幸存者看不见的障碍物后面刷特,但时间上无法控制,只会更慢,不会更快刷特...(几乎不卡特感)
	*/
	SetIsModeSpawnSpecial(1);
	
	SetIsModeSpawnSpecialInNormal(1); //只在正常模式下有效的子模式,只有1和2,具体的区别自己体验去.极少部分服务器若崩服请换另一种模式,理论上两种模式都是非常稳定的,但可能架不住某些服务器真的比较奇葩.
	
	SetSpecialInfectedAssault(true);    //特感刚产生是否立即会主动向幸存者发起袭击？？或许设置true后特感可能无法再蹲人，各有利弊吧.
	
	SetSpecialRespawnInterval(15);//设置特感再次产生的时间;
	SetMaxSpecials(15);//设置特感产生的最大数量;
	
	SetSmokerLimit(5);//设置smoker的最大数量,.
	SetBoomerLimit(5);//设置boomer的最大数量,.
	SetHunterLimit(5);//设置hunter的最大数量,.
	SetSpitterLimit(5);//设置spitter的最大数量,.
	SetJockeyLimit(5);//设置jockey的最大数量,.
	SetChargerLimit(5);//设置charger的最大数量,.

	g_TankEIST = true;//坦克存在游戏时是否继续刷特;true=刷特，false=不刷特
	HookEvent("round_start", OnRoundStart);
	HookEvent("tank_spawn", OnTankSpawn);
	HookEvent("tank_killed", OnTankDeath);
	HookEvent("player_death", OnPlayerDeath);
	HookEvent("player_disconnect", Event_PlayerDisconnect, EventHookMode_Pre);	
	HookEvent("round_end", OnRoundEnd, EventHookMode_Pre);  
}

/*下面这段代码仅供参考:
**内容是特感根据人数增加而增加
**小于四人时特感数量总是为4，大于四人时每增加一个玩家增加一个特感.
**如果想改小于四人时的特感数量和每增加一个玩家增加多少特感请看下面的注释;
**/

public Action OnPlayerStuck(int client)
{
	if(IsValidClient(client) && IsPlayerAlive(client))
	{
		if(GetClientTeam(client)==3 && IsFakeClient(client) && !IsTank(client)) KickClient(client,"感染者卡住踢出");
	}
	return Plugin_Continue;
}

public Action OnRoundStart( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	//开局后多少秒开始刷特,如果过了这个时间后还没有玩家离开开始区域，还是不会刷特，
	//然而如果玩家离开了安全区域，可是这个时间没过，也不会开始刷特.（默认5.0）
	CreateTimer( 5.0, Timer_DelaySpawnInfected);
	return Plugin_Continue;
}

public Action Timer_DelaySpawnInfected( Handle hTimer)
{
	SetIsLoadASI(true); 
}

public Action OnRoundEnd( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	SetIsLoadASI(false); 
	return Plugin_Continue;
}

public void OnMapEnd()
{
	SetIsLoadASI(false); 
}

public Action OnTankSpawn( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	int client = GetClientOfUserId(hEvent.GetInt( "userid" ));	
	if(IsValidClient(client) && GetClientTeam(client) == 3)
	{
		if(!g_TankEIST)
			SetIsLoadASI(false);  
	}
	return Plugin_Continue;
}

public Action OnTankDeath( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	int client = GetClientOfUserId(hEvent.GetInt( "userid" ));
	if (IsValidClient(client) && GetClientTeam(client)==3)
	{
		if(!g_TankEIST)
			CreateTimer( 0.2, Timer_DelayDeath);
	}
	return Plugin_Continue;
}

public Action Timer_DelayDeath( Handle hTimer)
{
	SetIsLoadASI(true); 
	for (int i = 1; i <= MaxClients; i++)
		if(IsTank(i) && IsPlayerAlive(i))
	{
		SetIsLoadASI(false);
		break;
	}
}

//这个是必需的,否则数量可能并不精准，这是由于系统的限制，目前只能通过这种方法控制精准度
public Action OnPlayerDeath( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	int client = GetClientOfUserId(hEvent.GetInt( "userid" ));
	if (IsValidClient(client) && IsFakeClient(client) && GetClientTeam(client)==3)
	{
		RequestFrame(Timer_KickBot, GetClientUserId(client));
	}
}

public void Timer_KickBot(any client) 
{
	client = GetClientOfUserId(client);
	if (client>0 && IsClientInGame(client)) 
	{
		if (IsFakeClient(client) && !IsClientInKickQueue(client)) KickClient(client);
	}
}

public void OnClientPutInServer(int client)
{
	RequestFrame(ClientPutInServer, GetClientUserId(client));
}

public void ClientPutInServer(any client) 
{
	client = GetClientOfUserId(client);
	if (IsValidClient(client) && !IsFakeClient(client))
		SteMaxSpecialsCount(true);
}


public void Event_PlayerDisconnect( Event hEvent, const char[] sName, bool bDontBroadcast )
{
	int client = GetClientOfUserId(hEvent.GetInt( "userid" ));
	if(!client || (IsClientConnected(client) && !IsClientInGame(client))) return; 
	if(!IsFakeClient(client))
	{
		CreateTimer( 0.2, Timer_DelayCheckDisconnect);
	}
	if(IsTank(client))
		SetIsLoadASI(true); 
}

public Action Timer_DelayCheckDisconnect( Handle hTimer, any UserID )
{
	SteMaxSpecialsCount(false);
}

public void SteMaxSpecialsCount(bool IsClientPutInServer)
{
	int RealPlayer_count;
	int AddSpecialFromSurvivor = 2;//此处改每增加一个玩家可增加多少个特感;
	int Specials_count = 6;//此处改的内容为游戏中玩家数量小于等于4时特感的固定数量为多少
	char line[128];
	for (int i = 1; i <= MaxClients; i++)
		if(IsClientInGame(i) && !IsFakeClient(i))
			++RealPlayer_count;
	
	if(RealPlayer_count<=4)
		SetMaxSpecials(Specials_count);
	else 
	{
		Specials_count = AddSpecialFromSurvivor * (RealPlayer_count - 4) + Specials_count;
		SetMaxSpecials(Specials_count);
	}
	
	if(IsClientPutInServer)
		Format(line, sizeof(line), "幸存者增加了,特感数量为%d特", Specials_count);
	else
		Format(line, sizeof(line), "幸存者减少了,特感数量为%d特", Specials_count);
	HUDSetLayout(HUD_SCORE_4, HUD_FLAG_ALIGN_LEFT|HUD_FLAG_BLINK|HUD_FLAG_COUNTDOWN_WARN, line);
	HUDPlace(HUD_SCORE_4,0.35,0.05,1.0,0.05);
	CreateTimer(5.0, delay_timer);
}

public Action delay_timer(Handle timer)
{
	if(HUDSlotIsUsed(HUD_SCORE_4))
		RemoveHUD(HUD_SCORE_4);
}

stock bool IsValidClient(int client)
{
	return (client > 0 && client <= MaxClients && IsClientInGame(client));
}

stock bool IsTank( int client )
{
	if( IsValidClient(client) && GetClientTeam( client ) == 3 )
		if( GetEntProp( client, Prop_Send, "m_zombieClass" ) == 8 )
			return true;
	
	return false;
}